package factory;

import argchecker.ArgChecker;
import extracting.CardImageExtractor;
import extracting.RankSuitExtractor;
import recognizing.ImageRecognizer;
import recognizing.RankRecognizer;
import recognizing.SuitRecognizer;

/** Интерфейс фабрики классов. */
public interface Factory {

    /**
     * Получить объект проверки входящих параметров приложения
     * @param args входящие параметры
     * @return объект проверки входящих параметров приложения
     */
    ArgChecker getArgsChecker(String[] args);

    /**
     * Получить объект распознавания ранга карты
     * @return объект распознавания ранга карты
     */
    RankRecognizer getRankRecognizer();

    /**
     * Получить объект распознавания масти карты
     * @return объект распознавания масти карты
     */
    SuitRecognizer getSuitRecognizer();

    /**
     * Получить объект для извлечения ранга/масти карты
     * @return объект для извлчения ранга/масти карты
     */
    RankSuitExtractor getRankSuitExtractor();

    /**
     * Получить объект извлечения изображений карт доски
     * @return объект извлечения изображений карт доски
     */
    CardImageExtractor getCardImageExtractor();

    /**
     * Получить объект распознавания изображений каталога
     * @return объект распознавания изображений каталога
     */
    ImageRecognizer getImageRecognizer();
}
