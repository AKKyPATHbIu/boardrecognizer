package factory;

import argchecker.ArgChecker;
import argchecker.ArgCheckerImpl;
import extracting.*;
import recognizing.*;

/** Реализация интерфейса {@link Factory} */
public class FactoryImpl implements Factory {

    @Override
    public ArgChecker getArgsChecker(String[] args) {
        return new ArgCheckerImpl(args);
    }

    @Override
    public RankRecognizer getRankRecognizer() {
        return new RankRecognizerImpl();
    }

    @Override
    public SuitRecognizer getSuitRecognizer() {
        return new SuitRecognizerImpl();
    }

    @Override
    public RankSuitExtractor getRankSuitExtractor() {
        return new RankSuitExtractorImpl();
    }

    @Override
    public CardImageExtractor getCardImageExtractor() {
        return new CardImageExtractorImpl();
    }

    @Override
    public ImageRecognizer getImageRecognizer() {
        return new ImageRecognizerImpl(this);
    }
}
