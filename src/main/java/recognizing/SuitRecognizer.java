package recognizing;

import enums.CardSuit;

import java.awt.image.BufferedImage;

/** Интерфейс распознавания масти. */
public interface SuitRecognizer {

    /**
     * Распознать масть из изображения
     * @param objImage изображение масти
     * @return масть из изображения
     */
    CardSuit recognize(BufferedImage objImage);
}
