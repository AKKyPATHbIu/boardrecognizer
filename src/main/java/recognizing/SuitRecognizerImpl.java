package recognizing;

import enums.CardSuit;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * Релизация интерфйеса {@link SuitRecognizer}
 */
public class SuitRecognizerImpl extends BaseRecognizer<CardSuit> implements SuitRecognizer {

    @Override
    protected void fillVocabulary() {
        try {
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("suit_H.png")), CardSuit.HART));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("suit_S.png")), CardSuit.SPADE));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("suit_C.png")), CardSuit.CLUB));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("suit_D.png")), CardSuit.DIAMOND));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
