package recognizing;

import enums.CardRank;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * Релизация интерфейса {@link RankRecognizer}
 */
public class RankRecognizerImpl extends BaseRecognizer<CardRank> implements RankRecognizer {

    @Override
    public void fillVocabulary() {
        try {
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_2.png")), CardRank.DEUCE));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_3.png")), CardRank.THREE));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_4.png")), CardRank.FOUR));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_5.png")), CardRank.FIVE));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_6.png")), CardRank.SIX));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_7.png")), CardRank.SEVEN));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_8.png")), CardRank.EIGHT));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_9.png")), CardRank.NINE));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_T.png")), CardRank.TEN));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_J.png")), CardRank.JACK));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_Q.png")), CardRank.QUEEN));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_K.png")), CardRank.KING));
            vocabulary.add(new SuitRank<>(ImageIO.read(accessFile("rank_A.png")), CardRank.ACE));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
