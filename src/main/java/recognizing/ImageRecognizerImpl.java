package recognizing;

import enums.CardRank;
import enums.CardSuit;
import extracting.CardImageExtractor;
import extracting.RankSuitExtractor;
import factory.Factory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

/** Реализация интерфеса {@link ImageRecognizer} */
public class ImageRecognizerImpl implements ImageRecognizer {

    /** Фабрика классов. */
    private final Factory factory;

    /** Фильтр для png-файлов. */
    private final FilenameFilter pngFilter = new FilenameFilter() {

        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(".png");
        }
    };

    /**
     * Конструктор
     * @param factory фабрика классов
     */
    public ImageRecognizerImpl(Factory factory) {
        this.factory = factory;
    }

    @Override
    public void printCardsIn(String path) {
        File dir = new File(path);

        CardImageExtractor cardExtractor = factory.getCardImageExtractor();
        RankRecognizer rankRecognizer = factory.getRankRecognizer();
        SuitRecognizer suitRecognizer = factory.getSuitRecognizer();
        RankSuitExtractor rankSuiteExtractor = factory.getRankSuitExtractor();

        StringBuilder boardString = new StringBuilder();

        for (File imageFile : dir.listFiles(pngFilter)) {
            BufferedImage image = readFile(imageFile);
            if (image == null) {
                continue;
            }

            BufferedImage[] cards = cardExtractor.extractCardsFrom(image);

            boardString.setLength(0);

            for (BufferedImage cardImage : cards) {
                CardRank cardRank = rankRecognizer.recognize(rankSuiteExtractor.extractRankImage(cardImage));
                CardSuit cardSuit = suitRecognizer.recognize(rankSuiteExtractor.extractSuitImage(cardImage));
                boardString.append(cardRank.getStringValue()).append(cardSuit.getStringValue());
            }
            System.out.println(String.format("%s - %s", imageFile.getName(), boardString.toString()));
        }
    }

    /**
     * Прочитать изображение из файла
     * @param imageFile файл, из которого необходимо прочитать изображение
     * @return изображение из файла
     */
    private BufferedImage readFile(File imageFile) {
        BufferedImage image = null;

        try {
            image = ImageIO.read(imageFile);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return image;
    }
}
