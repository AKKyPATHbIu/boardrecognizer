package recognizing;

import java.awt.image.BufferedImage;

/**
 * Параметризированный объект ранга/масти
 * @param <T> тип объекта
 */
public class SuitRank<T> {

    /** Изображение ранга/масти. */
    private final BufferedImage image;

    /** Значение типа объекта (ранг/масть). */
    private final T value;

    /**
     * Конструктор
     * @param image изображение объекта
     * @param value тип объекта (ранг/масть)
     */
    public SuitRank(BufferedImage image, T value) {
        this.image = image;
        this.value = value;
    }

    /**
     * Вычислить отличие текущего объекта от изображения
     * @param img сравниваемое с текущим изображение
     * @return отличие изображения от текущего объекта
     */
    public int computeDifferenceWith(BufferedImage img) {
        int width = image.getWidth() < img.getWidth() ? image.getWidth() : img.getWidth();
        int height = image.getHeight() < img.getHeight() ? image.getHeight() : img.getHeight();


        int result = 0;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (image.getRGB(x, y) != img.getRGB(x, y)) {
                    result++;
                }
            }
        }
        return result;
    }

    /**
     * Получить значение объекта (ранга/масти)
     * @return значение объекта (ранга/масти)
     */
    public T getValue() {
        return value;
    }
}
