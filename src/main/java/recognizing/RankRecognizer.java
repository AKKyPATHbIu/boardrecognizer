package recognizing;

import enums.CardRank;

import java.awt.image.BufferedImage;

/** Интерфейс распознавания ранга. */
public interface RankRecognizer {

    /**
     * Распознать ранг из изображения
     * @param objImage изображение ранга
     * @return ранг из изображения
     */
    CardRank recognize(BufferedImage objImage);
}
