package recognizing;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Базовый класс распознавания объекта
 */
public abstract class BaseRecognizer<T> {

    /**  Словарь для распознавания объекта. */
    protected final List<SuitRank<T>> vocabulary = new ArrayList<> ();

    /** Наполнить словарь для распознавания. */
    protected abstract void fillVocabulary();

    /** Конструктор по умолчанию. */
    public BaseRecognizer() {
        fillVocabulary();
    }

    /**
     * Распознать объект
     * @param objImage объект в графическом виде
     * @return тип объекта
     */
    public T recognize(BufferedImage objImage) {
        T result = vocabulary.get(0).getValue();
        int curDiff = vocabulary.get(0).computeDifferenceWith(objImage);
        for (int i = 1; i < vocabulary.size(); i++) {
            SuitRank<T> r = vocabulary.get(i);
            int diff = r.computeDifferenceWith(objImage);
            if (curDiff > diff) {
                result = r.getValue();
                curDiff = diff;
            }
        }
        return result;
    }

    /**
     * Прочитать файл ресурсов
     * @param resource имя файла ресурсов
     * @return входящий поток файла ресурсов
     */
    protected InputStream accessFile(String resource) {
        InputStream input = BaseRecognizer.class.getResourceAsStream("/resources/" + resource);
        if (input == null) {
            input = BaseRecognizer.class.getClassLoader().getResourceAsStream(resource);
        }
        return input;
    }
}
