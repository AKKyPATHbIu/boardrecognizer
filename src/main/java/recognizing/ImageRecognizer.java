package recognizing;

/** Интерфейс распознования карт в каталоге. */
public interface ImageRecognizer {

    /**
     * Распечатать распознанные карты в каталоге
     * @param path каталог с изображениями для распознавания
     */
    void printCardsIn(String path);
}
