import argchecker.ArgChecker;
import factory.Factory;
import factory.FactoryImpl;
import recognizing.ImageRecognizer;

import static java.lang.System.exit;

/**
 * Основной класс приложения
 */
public class Main {

    /** Фабрика классов. */
    private static Factory factory = new FactoryImpl();

    public static void main(String[] args) {
        ArgChecker argsChecker = factory.getArgsChecker(args);

        if (argsChecker.hasErrors()) {
            System.out.println(argsChecker.getErrorMessage());
            exit(1);
        }

        ImageRecognizer imageRecognizer = factory.getImageRecognizer();
        imageRecognizer.printCardsIn(args[0]);
        exit(0);
    }
}
