package enums;

/** Доступные ранги карт. */
public enum CardRank implements Printable {
    DEUCE("2"), THREE("3"), FOUR("4"), FIVE("5"), SIX("6"), SEVEN("7"), EIGHT("8"), NINE("9"), TEN("10"), JACK("J"), QUEEN("Q"), KING("K"), ACE("A");

    /** Строковое представление ранга. */
    private final String stringValue;

    /**
     * Конструктор
     * @param stringValue строковое представление ранга
     */
    CardRank(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String getStringValue() {
        return stringValue;
    }
}
