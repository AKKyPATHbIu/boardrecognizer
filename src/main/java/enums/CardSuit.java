package enums;

/** Доступные масти карт. */
public enum CardSuit implements Printable {
    SPADE("s"), HART("h"), CLUB("c") , DIAMOND("d");

    /** Строковое представление масти. */
    private final String stringValue;

    /**
     * Конструктор
     * @param stringValue строковое представление масти
     */
    CardSuit(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String getStringValue() {
        return stringValue;
    }
}
