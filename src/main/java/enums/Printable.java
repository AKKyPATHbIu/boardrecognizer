package enums;

/** Интерфейс получения текстового отображения объекта. */
public interface Printable {

    /**
     * Получить текстовое отображение объекта
     * @return текстовое отображение объекта
     */
    String getStringValue();
}
