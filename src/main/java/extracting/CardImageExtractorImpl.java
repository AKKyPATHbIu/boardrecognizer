package extracting;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс извлечения карт доски
 * Реализация интерфейса {@link CardImageExtractor}
 */
public class CardImageExtractorImpl implements CardImageExtractor {

    /** Координата верхней левой точки области с картами. */
    private final Point LEFT_TOP_CORNER = new Point(140, 584);

    /** Координата нижней правой точки области с картами. */
    private final Point BOTTOM_RIGHT_CORNER = new Point(500, 675);

    /** Координата верхней левой точки карты. */
    private final Point CARD_LEFT_TOP_CORNER = new Point(8, 7);

    /** Расстояние между картами. */
    private final int CARD_DISTANCE = 18;

    /** Ширина карты. */
    private final int CARD_WIDTH = 54;

    /** Высота карты. */
    private final int CARD_HEIGHT = 80;

    /** Исходное изображение со всеми картами. */
    private final BufferedImage board;

    /** Осветленное изображение со всеми картами. */
    private final BufferedImage clarifiedBoard;

    /** Конструктор по умолчанию. */
    public CardImageExtractorImpl() {
        board = new BufferedImage(BOTTOM_RIGHT_CORNER.x - LEFT_TOP_CORNER.x, BOTTOM_RIGHT_CORNER.y - LEFT_TOP_CORNER.y,
                BufferedImage.TYPE_BYTE_GRAY);
        clarifiedBoard = new BufferedImage(BOTTOM_RIGHT_CORNER.x - LEFT_TOP_CORNER.x, BOTTOM_RIGHT_CORNER.y - LEFT_TOP_CORNER.y,
                BufferedImage.TYPE_BYTE_GRAY);
    }

    @Override
    public BufferedImage[] extractCardsFrom(BufferedImage board) {

        this.board.getGraphics().drawImage(board.getSubimage(LEFT_TOP_CORNER.x, LEFT_TOP_CORNER.y,
                this.board.getWidth(), this.board.getHeight()), 0, 0, null);

        clarifiedBoard.getGraphics().drawImage(board.getSubimage(LEFT_TOP_CORNER.x, LEFT_TOP_CORNER.y,
                this.board.getWidth(), this.board.getHeight()), 0, 0, null);

        RescaleOp rescaleOp = new RescaleOp(2f, 0, null);
        rescaleOp.filter(clarifiedBoard, clarifiedBoard);

        List<BufferedImage> cards = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            BufferedImage cardImage = new BufferedImage(CARD_WIDTH, CARD_HEIGHT, BufferedImage.TYPE_BYTE_BINARY);
            int x = CARD_LEFT_TOP_CORNER.x + (CARD_DISTANCE + CARD_WIDTH) * i;
            cardImage.getGraphics().drawImage(this.board.getSubimage(x, CARD_LEFT_TOP_CORNER.y, CARD_WIDTH, CARD_HEIGHT),
                    0, 0, null);

            if (hasWhitePixel(cardImage)) {
                cards.add(cardImage);
            } else {
                cardImage.getGraphics().drawImage(this.clarifiedBoard.getSubimage(x, CARD_LEFT_TOP_CORNER.y, CARD_WIDTH, CARD_HEIGHT),
                        0, 0, null);
                if (hasWhitePixel(cardImage)) {
                    cards.add(cardImage);
                } else {
                    break;
                }
            }
        }
        return cards.toArray(new BufferedImage[cards.size()]);
    }

    /**
     * Проверить, что область с картой содержит белый пиксель
     * @param cardImage изображение с картой
     * @return true, если область содержит белый пиксель
     */
    private boolean hasWhitePixel(BufferedImage cardImage) {
        return cardImage.getRGB(5, 70) == ColorUtil.WHITE_PIXEL_CODE;
    }
}
