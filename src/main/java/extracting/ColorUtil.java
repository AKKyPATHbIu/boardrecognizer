package extracting;

public class ColorUtil {

    /** Код белого пикселя. */
    public static int WHITE_PIXEL_CODE = 0xFFFFFFFF;

    /** Код черного пикселя. */
    public static int BLACK_PIXEL_CODE = 0xFF000000;
}
