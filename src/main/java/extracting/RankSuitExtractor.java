package extracting;

import java.awt.image.BufferedImage;

/**
 * Интерфейс извлечения изображения ранга, масти карты
 */
public interface RankSuitExtractor {

    /**
     * Извлечь изображение ранга карты
     * @param cardImage изображение карты
     * @return изображение ранга карты
     */
    BufferedImage extractRankImage(BufferedImage cardImage);

    /**
     * Извлечь изображение масти карты
     * @param cardImage изображение карты
     * @return изображение масти карты
     */
    BufferedImage extractSuitImage(BufferedImage cardImage);
}
