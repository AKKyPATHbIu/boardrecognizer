package extracting;

import java.awt.image.BufferedImage;

/**
 * Класс извлечения изображения ранга, масти карты
 */
public class RankSuitExtractorImpl implements RankSuitExtractor {

    @Override
    public BufferedImage extractSuitImage(BufferedImage cardImage) {
        int cardWidth = cardImage.getWidth();
        int cardHeight = cardImage.getHeight();

        int halfHeight = cardHeight / 2;
        int halfWidth = cardWidth / 2;
        int bottomY = 0;

        for (int y = cardHeight - 1; y > halfHeight; y--) {
            for (int x = cardWidth - 1; x > halfWidth; x--) {
                if (cardImage.getRGB(x, y) == ColorUtil.BLACK_PIXEL_CODE) {
                    bottomY = y;
                    y = halfHeight;
                    break;
                }
            }
        }

        int topY = cardHeight;

        int rightX = 0;

        for (int y = bottomY; y > halfHeight; y--) {
            boolean isBlackPixelFound = false;
            for (int x = cardWidth - 1; x > halfWidth; x--) {
                if (cardImage.getRGB(x, y) == ColorUtil.BLACK_PIXEL_CODE) {
                    isBlackPixelFound = true;
                    topY = topY > y ? y : topY;
                    if (rightX < x) {
                        rightX = x;
                    }
                    break;
                }
            }
            if (!isBlackPixelFound) {
                break;
            }
        }

        int leftX = cardWidth - 1;

        for (int y = topY; y < bottomY; y++) {
            for (int x = 0; x < rightX; x++) {
                if (cardImage.getRGB(x, y) == ColorUtil.BLACK_PIXEL_CODE) {
                    leftX = leftX > x ? x : leftX;
                    break;
                }
            }
        }

        return cardImage.getSubimage(leftX, topY, rightX - leftX + 1, bottomY - topY + 1);
    }

    @Override
    public BufferedImage extractRankImage(BufferedImage cardImage) {
        int cardWidth = cardImage.getWidth();
        int cardHeight = cardImage.getHeight();

        int halfWidth = cardWidth / 2;
        int topY = 0;

        for (int y = 0; y < cardHeight; y++) {
            for (int x = 0; x < halfWidth; x++) {
                if (cardImage.getRGB(x, y) == ColorUtil.BLACK_PIXEL_CODE) {
                    topY = y;
                    y = cardHeight;
                    break;
                }
            }
        }

        int bottomY = 0;
        int leftX = halfWidth;

        for (int y = topY; y < cardHeight; y++) {
            boolean isBlackPixelFound = false;
            for (int x = 0; x < halfWidth; x++) {
                if (cardImage.getRGB(x, y) == ColorUtil.BLACK_PIXEL_CODE) {
                    bottomY = y;
                    leftX = leftX > x ? x : leftX;
                    isBlackPixelFound = true;
                    break;
                }
            }
            if (!isBlackPixelFound) {
                break;
            }
        }

        int rightX = 0;

        for (int y = topY; y <= bottomY; y++) {
            for (int x = halfWidth; x > rightX; x--) {
                if (cardImage.getRGB(x, y) == ColorUtil.BLACK_PIXEL_CODE) {
                    rightX = rightX < x ? x : rightX;
                    break;
                }
            }
        }

        return cardImage.getSubimage(leftX, topY, rightX - leftX + 1, bottomY - topY + 1);
    }
}
