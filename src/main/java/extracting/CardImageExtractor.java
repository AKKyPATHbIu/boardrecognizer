package extracting;

import java.awt.image.BufferedImage;

/**
 * Интерфейс извлечения изображений карт доски
 */
public interface CardImageExtractor {

    /**
     * Извлечь карты доски
     * @param boardImage доска с картами
     * @return изображения карт доски по порядку
     */
    BufferedImage[] extractCardsFrom(BufferedImage boardImage);
}
