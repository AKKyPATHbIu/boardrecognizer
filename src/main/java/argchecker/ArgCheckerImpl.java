package argchecker;

import java.io.File;

/**
 * Реализация интерфейса {@link ArgChecker}
 */
public class ArgCheckerImpl implements ArgChecker {

    /** Текст ошибки. */
    private String errorMessage = null;

    /** Сообщение, когда не указан параметр. */
    private final String ERR_NEED_PARAM_MSG = "Укажите путь к директории в качестве параметра...";

    /** Сообщение, когда параметр неверный. */
    private final String ERR_DIRECTORY_NOT_EXISTS_MSG = "Директория %s не найдена...";

    /**
     * Конструктор
     * @param args входящие параметры
     */
    public ArgCheckerImpl(String[] args) {
        if (args == null || args.length < 1) {
            errorMessage = ERR_NEED_PARAM_MSG;
        } else {
            String path = args[0];
            if (!new File(path).isDirectory()) {
                errorMessage = String.format(ERR_DIRECTORY_NOT_EXISTS_MSG, path);
            }
        }
    }

    @Override
    public boolean hasErrors() {
        return errorMessage != null;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
