package argchecker;

/** Интерфейс проверки входных параметров. */
public interface ArgChecker {

    /**
     * Проверить на наличие ошибок
     * @return true в случае наличия ошибок
     */
    boolean hasErrors();

    /**
     * Получить текстовое описание ошибки
     * @return текстовое описание ошибки
     */
    String getErrorMessage();
}
