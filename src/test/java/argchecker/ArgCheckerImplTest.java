package argchecker;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.junit.Assert.*;

public class ArgCheckerImplTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    /** Сообщение, когда не указан параметр. */
    private final String ERR_NEED_PARAM_MSG = "Укажите путь к директории в качестве параметра...";

    /** Сообщение, когда параметр неверный. */
    private final String ERR_DIRECTORY_NOT_EXISTS_MSG = "Директория %s не найдена...";

    @Test
    public void test() {
        ArgChecker argChecker = new ArgCheckerImpl(null);
        assertTrue(argChecker.hasErrors());
        assertEquals(ERR_NEED_PARAM_MSG, argChecker.getErrorMessage());

        argChecker = new ArgCheckerImpl(new String[0]);
        assertTrue(argChecker.hasErrors());
        assertEquals(ERR_NEED_PARAM_MSG, argChecker.getErrorMessage());

        String notExistDir = "abcdefg";

        argChecker = new ArgCheckerImpl(new String[] { notExistDir });
        assertTrue(argChecker.hasErrors());
        assertEquals(String.format(ERR_DIRECTORY_NOT_EXISTS_MSG, notExistDir), argChecker.getErrorMessage());

        argChecker = new ArgCheckerImpl(new String[] { folder.getRoot().getAbsolutePath() });
        assertFalse(argChecker.hasErrors());
        assertNull(argChecker.getErrorMessage());
    }
}