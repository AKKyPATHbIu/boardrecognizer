package recognizing;

import enums.CardSuit;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;
import static recognizing.Utils.loadImage;

public class SuitRecognizerImplTest {

    @Test
    public void testRecognition() throws IOException {
        SuitRecognizer suitRecognizer = new SuitRecognizerImpl();
        assertEquals(CardSuit.SPADE, suitRecognizer.recognize(loadImage("suit_S.png")));
        assertEquals(CardSuit.HART, suitRecognizer.recognize(loadImage("suit_H.png")));
        assertEquals(CardSuit.CLUB, suitRecognizer.recognize(loadImage("suit_C.png")));
        assertEquals(CardSuit.DIAMOND, suitRecognizer.recognize(loadImage("suit_D.png")));
    }
}