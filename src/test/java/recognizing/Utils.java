package recognizing;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/** Вспомагательная утилита для тестов. */
public class Utils {

    /**
     * Загрузить изображение
     * @param fileName имя файла
     * @return изображение
     */
    public static BufferedImage loadImage(String fileName) throws IOException  {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File imageFile = new File(classLoader.getResource(fileName).getFile());
        return ImageIO.read(imageFile);
    }
}
