package recognizing;

import factory.Factory;
import factory.FactoryImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class ImageRecognizerImplTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    Factory factory = new FactoryImpl();

    private final String IMGS_PATH = "src/test/resources/imgs";

    private final String[] expectedOutput = new String[] {
            "20180821_115544.512_0x1CFF023A.png - JcAs5c2c",
            "20180821_115640.484_0x1CFF023A.png - 8dAsQc2c8s",
            "20180821_120002.260_0x1CFF023A.png - 4hKh4dAs",
            "20180821_120241.032_0x1CFF023A.png - 4sJdKdKc",
            "20180821_121036.744_0x1CFF023A.png - 4cJs6c"};

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void printCardsIn() {
        ImageRecognizer recognizer = new ImageRecognizerImpl(factory);
        recognizer.printCardsIn(IMGS_PATH);
        String output = outContent.toString();
        assertFalse(output.isEmpty());
        for (String resultStr : expectedOutput) {
            assertTrue(output.contains(resultStr));
        }
    }
}