package recognizing;

import enums.CardRank;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static recognizing.Utils.loadImage;

public class RankRecognizerImplTest {

    @Test
    public void testRecognition() throws IOException {
        RankRecognizer rankRecognizer = new RankRecognizerImpl();
        assertEquals(CardRank.DEUCE, rankRecognizer.recognize(loadImage("rank_2.png")));
        assertEquals(CardRank.THREE, rankRecognizer.recognize(loadImage("rank_3.png")));
        assertEquals(CardRank.FOUR, rankRecognizer.recognize(loadImage("rank_4.png")));
        assertEquals(CardRank.FIVE, rankRecognizer.recognize(loadImage("rank_5.png")));
        assertEquals(CardRank.SIX, rankRecognizer.recognize(loadImage("rank_6.png")));
        assertEquals(CardRank.SEVEN, rankRecognizer.recognize(loadImage("rank_7.png")));
        assertEquals(CardRank.EIGHT, rankRecognizer.recognize(loadImage("rank_8.png")));
        assertEquals(CardRank.NINE, rankRecognizer.recognize(loadImage("rank_9.png")));
        assertEquals(CardRank.TEN, rankRecognizer.recognize(loadImage("rank_T.png")));
        assertEquals(CardRank.JACK, rankRecognizer.recognize(loadImage("rank_J.png")));
        assertEquals(CardRank.QUEEN, rankRecognizer.recognize(loadImage("rank_Q.png")));
        assertEquals(CardRank.KING, rankRecognizer.recognize(loadImage("rank_K.png")));
        assertEquals(CardRank.ACE, rankRecognizer.recognize(loadImage("rank_A.png")));
    }
}