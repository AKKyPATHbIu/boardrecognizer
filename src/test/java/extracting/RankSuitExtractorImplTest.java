package extracting;

import enums.CardRank;
import enums.CardSuit;
import factory.Factory;
import factory.FactoryImpl;
import org.junit.Test;
import recognizing.RankRecognizer;
import recognizing.SuitRecognizer;
import recognizing.Utils;

import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class RankSuitExtractorImplTest {

    Factory factory = new FactoryImpl();

    @Test
    public void test() throws IOException {
        RankSuitExtractor extractor = new RankSuitExtractorImpl();
        BufferedImage cardImage = Utils.loadImage("card.png");

        BufferedImage rankImage = extractor.extractRankImage(cardImage);
        RankRecognizer rankRecognizer = factory.getRankRecognizer();
        assertEquals(CardRank.FIVE, rankRecognizer.recognize(rankImage));

        BufferedImage suitImage = extractor.extractSuitImage(cardImage);
        SuitRecognizer suitRecognizer = factory.getSuitRecognizer();
        assertEquals(CardSuit.HART, suitRecognizer.recognize(suitImage));
    }
}