package factory;

import argchecker.ArgChecker;
import argchecker.ArgCheckerImpl;
import extracting.*;
import org.junit.Test;
import recognizing.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FactoryImplTest {

    /** Фабрика классов. */
    private final Factory factory = new FactoryImpl();

    @Test
    public void getArgsChecker() {
        ArgChecker argChecker = factory.getArgsChecker(new String[0]);
        assertNotNull(argChecker);
        assertTrue(argChecker instanceof ArgCheckerImpl);
    }

    @Test
    public void getRankRecognizer() {
        RankRecognizer rankRecognizer = factory.getRankRecognizer();
        assertNotNull(rankRecognizer);
        assertTrue(rankRecognizer instanceof RankRecognizerImpl);
    }

    @Test
    public void getSuitRecognizer() {
        SuitRecognizer suitRecognizer = factory.getSuitRecognizer();
        assertNotNull(suitRecognizer);
        assertTrue(suitRecognizer instanceof SuitRecognizerImpl);
    }

    @Test
    public void getRankSuitExtractor() {
        RankSuitExtractor extractor = factory.getRankSuitExtractor();
        assertNotNull(extractor);
        assertTrue(extractor instanceof RankSuitExtractorImpl);
    }

    @Test
    public void getCardImageExtractor() {
        CardImageExtractor cardImageExtractor = factory.getCardImageExtractor();
        assertNotNull(cardImageExtractor);
        assertTrue(cardImageExtractor instanceof CardImageExtractorImpl);
    }

    @Test
    public void getImageRecognizer() {
        ImageRecognizer imageRecognizer = factory.getImageRecognizer();
        assertNotNull(imageRecognizer);
        assertTrue(imageRecognizer instanceof ImageRecognizerImpl);
    }
}