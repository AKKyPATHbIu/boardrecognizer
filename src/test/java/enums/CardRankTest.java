package enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CardRankTest {

    @Test
    public void testCount() {
        assertEquals(13, CardRank.values().length);
    }

    @Test
    public void getStringValue() {
        assertEquals("2", CardRank.DEUCE.getStringValue());
        assertEquals("3", CardRank.THREE.getStringValue());
        assertEquals("4", CardRank.FOUR.getStringValue());
        assertEquals("5", CardRank.FIVE.getStringValue());
        assertEquals("6", CardRank.SIX.getStringValue());
        assertEquals("7", CardRank.SEVEN.getStringValue());
        assertEquals("8", CardRank.EIGHT.getStringValue());
        assertEquals("9", CardRank.NINE.getStringValue());
        assertEquals("10", CardRank.TEN.getStringValue());
        assertEquals("J", CardRank.JACK.getStringValue());
        assertEquals("Q", CardRank.QUEEN.getStringValue());
        assertEquals("K", CardRank.KING.getStringValue());
        assertEquals("A", CardRank.ACE.getStringValue());
    }
}