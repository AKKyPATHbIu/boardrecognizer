package enums;

import org.junit.Test;

import static org.junit.Assert.*;

public class CardSuitTest {

    @Test
    public void testLength() {
        assertEquals(4, CardSuit.values().length);
    }

    @Test
    public void getStringValue() {
        assertEquals("s", CardSuit.SPADE.getStringValue());
        assertEquals("h", CardSuit.HART.getStringValue());
        assertEquals("d", CardSuit.DIAMOND.getStringValue());
        assertEquals("c", CardSuit.CLUB.getStringValue());
    }
}